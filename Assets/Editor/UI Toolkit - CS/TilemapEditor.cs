using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;


public class TilemapEditor : EditorWindow
{
    bool erase;
    bool paint; 
    ZombieGrunt grunt;
    TextField zombieNameTextField;
    TextField tilemapFileName;
    Slider zombieHealthSlider;
    EnumField zombieStartingStateEnum;
    Vector3Field zombiePositionField;

    TextField zombieGruntBackStory;
    IntegerField zombieGruntLives;

    Texture mapTexture;

    [MenuItem("Tools/Tilemap Editor")]
    public static void ShowExample()
    {
        TilemapEditor wnd = GetWindow<TilemapEditor>();
        wnd.titleContent = new GUIContent("Tilemap Editor");
    }

    private void SetVisualElementBorder(VisualElement element, float width, Color color)
    {
        element.style.borderTopWidth = element.style.borderBottomWidth =
            element.style.borderLeftWidth = element.style.borderRightWidth = width;
        element.style.borderTopColor = element.style.borderBottomColor =
            element.style.borderLeftColor = element.style.borderRightColor = color;
    }


    public void CreateGUI()
    {
        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;

        VisualElement mainPanel = new VisualElement();
        mainPanel.style.flexGrow = 1.0f;
        mainPanel.style.flexDirection = FlexDirection.Row;
        SetVisualElementBorder(mainPanel, 5.0f, Color.white);
        root.Add(mainPanel);


        #region Left Panel
        VisualElement leftPanel = new VisualElement();
        leftPanel.style.flexGrow = 0.25f;
        leftPanel.style.flexDirection = FlexDirection.Column;
        //SetVisualElementBorder(leftPanel, 5.0f, Color.blue);
        mainPanel.Add(leftPanel);

        //leftPanel.Add(new Label("Tilemaps: "));

        // Create some list of data, here simply numbers in interval [1, 1000]
        //const int itemCount = 3;
        //var items = new List<string>(itemCount);
        //for (int i = 1; i <= itemCount; i++)
        //items.Add(i.ToString());
        //items.Add("Dungeon Tilemap");
        //items.Add("Overworld Tilemap");

        // The "makeItem" function will be called as needed
        // when the ListView needs more items to render
        //Func<VisualElement> makeItem = () => new Label();

        // As the user scrolls through the list, the ListView object
        // will recycle elements created by the "makeItem"
        // and invoke the "bindItem" callback to associate
        // the element with the matching data item (specified as an index in the list)
        //Action<VisualElement, int> bindItem = (e, i) => (e as Label).text = items[i];

        // Provide the list view with an explict height for every row
        // so it can calculate how many items to actually display
        //const int itemHeight = 16;

        //var listView = new ListView(items, itemHeight, makeItem, bindItem);

        //listView.selectionType = SelectionType.Multiple;

        //listView.onItemChosen += obj => Debug.Log(obj);
        //listView.onSelectionChanged += objects => Debug.Log(objects);

        //listView.style.flexGrow = 1.0f;

        //leftPanel.Add(listView);

        //Old one is above

        //Foldout zombieFoldout2 = new Foldout();
        //zombieFoldout2.text = "Current Tilemap properties";
        //leftPanel.Add(zombieFoldout2);

        //var zombieNameTextField3 = new TextField("File Name: ");
        //zombieNameTextField3.RegisterCallback<ChangeEvent<string>>(
        //    (_evt) =>
        //    {
        //        if (grunt != null) grunt.name = _evt.newValue;
        //    }
        //);
        //zombieFoldout2.Add(zombieNameTextField3);

        //var zombieNameTextField4 = new TextField("File Location: ");
        //zombieNameTextField4.RegisterCallback<ChangeEvent<string>>(
        //    (_evt) =>
        //    {
        //        if (grunt != null) grunt.name = _evt.newValue;
        //    }
        //);
        //zombieFoldout2.Add(zombieNameTextField4);

        leftPanel.Add(new Label("Scriptable objects: "));

        // Create some list of data, here simply numbers in interval [1, 1000]
        const int itemCount2 = 3;
        var items2 = new List<string>(itemCount2);
        //for (int i = 1; i <= itemCount; i++)
        //items.Add(i.ToString());
        //items2.Add("Barrel");

        items2.Add("Barrel");
        items2.Add("Steel Barrel");


        // The "makeItem" function will be called as needed
        // when the ListView needs more items to render
        Func<VisualElement> makeItem2 = () => new Label();

        // As the user scrolls through the list, the ListView object
        // will recycle elements created by the "makeItem"
        // and invoke the "bindItem" callback to associate
        // the element with the matching data item (specified as an index in the list)
        Action<VisualElement, int> bindItem2 = (e, i) => (e as Label).text = items2[i];

        // Provide the list view with an explict height for every row
        // so it can calculate how many items to actually display
        const int itemHeight2 = 16;

        var listView2 = new ListView(items2, itemHeight2, makeItem2, bindItem2);

        listView2.selectionType = SelectionType.Multiple;

        listView2.onItemChosen += obj => Debug.Log(obj);
        listView2.onSelectionChanged += objects => Debug.Log(objects);

        listView2.style.flexGrow = 1.0f;

        leftPanel.Add(listView2);

        ObjectField tilemapObject= new ObjectField("Tile Selector");
        tilemapObject.objectType = typeof(TilemapItem);
        //zombieGruntObjectField.RegisterCallback<ChangeEvent<Object>>(OnZombieObjectChanged);
        leftPanel.Add(tilemapObject);

        //ObjectField zombieGruntObjectField = new ObjectField("Zombie Grunt");
        //zombieGruntObjectField.objectType = typeof(ZombieGrunt);
        //zombieGruntObjectField.RegisterCallback<ChangeEvent<Object>>(OnZombieObjectChanged);
        //leftPanel.Add(zombieGruntObjectField);

        //leftPanel.Add(new Label("Base Zombie Info"));

        //Foldout zombieFoldout = new Foldout();
        //zombieFoldout.text = "Scriptable objects properties";
        //leftPanel.Add(zombieFoldout);

        //zombieNameTextField = new TextField("File Name: ");
        //zombieNameTextField.RegisterCallback<ChangeEvent<string>>(
        //    (_evt) =>
        //    {
        //        if (grunt != null) grunt.name = _evt.newValue;
        //    }
        //);
        //zombieFoldout.Add(zombieNameTextField);

        //var zombieNameTextField2 = new TextField("File Location: ");
        //zombieNameTextField2.RegisterCallback<ChangeEvent<string>>(
        //    (_evt) =>
        //    {
        //        if (grunt != null) grunt.name = _evt.newValue;
        //    }
        //);
        //zombieFoldout.Add(zombieNameTextField2);

        // Add Health as a Slider
        //zombieHealthSlider = new Slider("Health: ", 0.0f, 100);
        //zombieHealthSlider.RegisterCallback<ChangeEvent<float>>(
        //    (_evt) =>
        //    {
        //        if (grunt != null) grunt.health = _evt.newValue;
        //    }
        //);
        //zombieFoldout.Add(zombieHealthSlider);

        //zombieStartingStateEnum = new EnumField("Starting State: ", Zombie.States.None);
        //zombieStartingStateEnum.RegisterValueChangedCallback<System.Enum>(
        //    (_evt) =>
        //    {
        //        if (grunt != null) grunt.startingState = (Zombie.States)_evt.newValue;
        //    }
        //);
        //zombieFoldout.Add(zombieStartingStateEnum);

        //zombiePositionField = new Vector3Field("Tile Position: ");
        //zombiePositionField.RegisterCallback<ChangeEvent<Vector3>>(
        //    (_evt) =>
        //    {
        //        if (grunt != null) grunt.position = _evt.newValue;
        //    }
        //);
        //zombieFoldout.Add(zombiePositionField);

        #endregion

        #region Right Panel
        VisualElement rightPanel = new VisualElement();
        rightPanel.style.flexGrow = 1.0f;
        rightPanel.style.flexDirection = FlexDirection.Column;
        //SetVisualElementBorder(rightPanel, 5.0f, Color.red);
        mainPanel.Add(rightPanel);

        #region Top Right Panel



        VisualElement topRightPanel = new VisualElement();
        topRightPanel.style.flexGrow = 1.0f;
        topRightPanel.style.flexDirection = FlexDirection.Column;
        //SetVisualElementBorder(topRightPanel, 5.0f, Color.green);
        rightPanel.Add(topRightPanel);

        mapTexture = Resources.Load<Texture>("Taco");


        topRightPanel.Add(new Label("Tilemap: "));

        //zombieGruntBackStory = new TextField("Back Story: ")
        //{
        //    multiline = true,
        //    viewDataKey = "input"
        //};
        //zombieGruntBackStory.RegisterCallback<ChangeEvent<string>>(
        //    (_evt) =>
        //    {
        //        if (grunt != null) grunt.backStory = _evt.newValue;
        //    }
        //);
        //topRightPanel.Add(zombieGruntBackStory);

        //zombieGruntLives = new IntegerField("Lives: ");
        //zombieGruntLives.RegisterCallback<ChangeEvent<int>>(
        //    (_evt) =>
        //    {
        //        if (grunt != null && _evt.newValue > 0)
        //        {
        //            grunt.lives = _evt.newValue;
        //        }
        //        else if (grunt != null)
        //        {
        //            zombieGruntLives.value = grunt.lives;
        //        }
        //    }
        //);
        //topRightPanel.Add(zombieGruntLives);

        //Button randomButton = new Button(Clicked)
        //{
        //    text = "Click Me",
        //    style =
        //    {
        //        width = 100,
        //        height = 50
        //    }
        //};
        //topRightPanel.Add(randomButton);

        //mapTexture = Resources.Load<Texture>("Taco");

        // Map layout

        Button paintButton = new Button(EraseClicked)
        {
            text = "Paint tile",
            style =
            {
                width = 100,
                height = 50
            }
        };
        topRightPanel.Add(paintButton);

        Button eraseButton = new Button(PaintClicked)
        {
            text = "Erase tile",
            style =
            {
                width = 100,
                height = 50
            }
        };
        topRightPanel.Add(eraseButton);

        for (int y = 0; y < 10; y++)
        {
            VisualElement rowPanel = new VisualElement();
            rowPanel.style.flexDirection = FlexDirection.Row;
            //SetVisualElementBorder(rowPanel, 2, Color.cyan);
            topRightPanel.Add(rowPanel);
            for (int x = 0; x < 10; x++)
            {
                Box box = new Box()
                {
                    style =
                    {
                        minWidth = 64,
                        minHeight = 64,
                        maxWidth = 64,
                        maxHeight = 64
                    }
                };

                SetVisualElementBorder(box, 1, Color.black);

                box.RegisterCallback<MouseUpEvent>(OnMouseUpEvent);

                Image image = new Image();
                image.style.flexShrink = 1.0f;
                image.image = mapTexture;
                box.Add(image);

                rowPanel.Add(box);
            }
        }

        #endregion

        #region Bottom Right Panel
        VisualElement bottomRightPanel = new VisualElement();
        bottomRightPanel.style.flexGrow = 1.0f;
        bottomRightPanel.style.minHeight = 200.0f;
        bottomRightPanel.style.flexDirection = FlexDirection.Column;
        //SetVisualElementBorder(bottomRightPanel, 5.0f, Color.yellow);

        //bottomRightPanel.generateVisualContent += GenerateMeshData;

        rightPanel.Add(bottomRightPanel);

        //rightPanel.Add(new Label("Tilemap Options: "));

        //var fileLoc1 = new TextField("Tilemap Name: ");
        //fileLoc1.RegisterCallback<ChangeEvent<string>>(
        //    (_evt) =>
        //    {
        //        if (grunt != null) grunt.name = _evt.newValue;
        //    }
        //);
        //rightPanel.Add(fileLoc1);

        ////rightPanel.Add(new Label("Tilemap Options: "));

        //var fileLoc = new TextField("Tilemap Location: ");
        //fileLoc.RegisterCallback<ChangeEvent<string>>(
        //    (_evt) =>
        //    {
        //        if (grunt != null) grunt.name = _evt.newValue;
        //    }
        //);
        //rightPanel.Add(fileLoc);

        //Button randomButton = new Button(Clicked)
        //{
        //    text = "Save Tilemap",
        //    style =
        //    {
        //        width = 100,
        //        height = 50
        //    }
        //};
        //rightPanel.Add(randomButton);

        //Button randomButton2 = new Button(Clicked)
        //{
        //    text = "Load Tilemap",
        //    style =
        //    {
        //        width = 100,
        //        height = 50
        //    }
        //};
        //rightPanel.Add(randomButton2);

        //Button randomButton3 = new Button(Clicked)
        //{
        //    text = "New Tilemap",
        //    style =
        //    {
        //        width = 100,
        //        height = 50
        //    }
        //};
        //rightPanel.Add(randomButton3);
        #endregion

        #endregion
    }

    //private void GenerateMeshData(MeshGenerationContext ctx)
    //{
    //    MeshWriteData data = ctx.Allocate(4, 6);
    //    data.SetNextVertex(new Vertex()
    //    {
    //        position = new Vector3(0, 0, Vertex.nearZ),
    //        tint = Color.red
    //    });
    //    data.SetNextVertex(new Vertex()
    //    {
    //        position = new Vector3(ctx.visualElement.worldBound.width, 0, Vertex.nearZ),
    //        tint = Color.green
    //    });
    //    data.SetNextVertex(new Vertex()
    //    {
    //        position = new Vector3(ctx.visualElement.worldBound.width, ctx.visualElement.worldBound.height, Vertex.nearZ),
    //        tint = Color.blue
    //    });
    //    data.SetNextVertex(new Vertex()
    //    {
    //        position = new Vector3(0, ctx.visualElement.worldBound.height, Vertex.nearZ),
    //        tint = Color.yellow
    //    });

    //    data.SetNextIndex(0);
    //    data.SetNextIndex(1);
    //    data.SetNextIndex(2);

    //    data.SetNextIndex(0);
    //    data.SetNextIndex(2);
    //    data.SetNextIndex(3);
    //}

    private void OnMouseUpEvent(MouseUpEvent _event)
    {
        Image image = _event.target as Image;
        //if (image != null && erase == true && paint == false)
        if (image != null)
        {
            VisualElement parent = image.parent;
            parent.Clear();
        }
        else if (image != null && erase == false && paint == true)
        {
            //Image image = new Image();
            //image.style.flexShrink = 1.0f;
            //image.image = mapTexture;

            VisualElement parent = image.parent;
            //parent.Insert(image);
            }
        else
        {
            Debug.Log("Something went wrong in the mouse up event.");
        }
    }

    private void EraseClicked()
    {
        Debug.Log("EraseClicked");

        erase = true;
        paint = false;

        //Grid grid = new Grid(4, 2, 10f);
    }

    private void PaintClicked()
    {
        Debug.Log("PaintClicked");

        erase = false;
        paint = true;
    }

    //private void OnZombieObjectChanged(ChangeEvent<Object> _evt)
    //{
    //    grunt = _evt.newValue as ZombieGrunt;
    //    if (grunt != null)
    //    {
    //        zombieNameTextField.value = grunt.name;
    //        zombieHealthSlider.value = grunt.health;
    //        zombieStartingStateEnum.value = grunt.startingState;
    //        zombiePositionField.value = grunt.position;
    //        zombieGruntLives.value = grunt.lives;
    //        zombieGruntBackStory.value = grunt.backStory;
    //    }
    //    else
    //    {
    //        zombieNameTextField.value = "";
    //        zombieHealthSlider.value = 0.0f;
    //        zombieStartingStateEnum.value = Zombie.States.None;
    //        zombiePositionField.value = Vector3.zero;
    //        zombieGruntLives.value = 0;
    //        zombieGruntBackStory.value = "";
    //    }
    //}

    public static Vector3 GetMouseWorldPosition()
    {
        Vector3 vec = GetMouseWorldPositionWithZ(Input.mousePosition, Camera.main);
        vec.z = 0f;
        return vec;
    }
    public static Vector3 GetMouseWorldPositionWithZ()
    {
        return GetMouseWorldPositionWithZ(Input.mousePosition, Camera.main);
    }
    public static Vector3 GetMouseWorldPositionWithZ(Camera worldCamera)
    {
        return GetMouseWorldPositionWithZ(Input.mousePosition, worldCamera);
    }
    public static Vector3 GetMouseWorldPositionWithZ(Vector3 screenPosition, Camera worldCamera)
    {
        Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
        return worldPosition;
    }

    //BreakPoint


}