using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TilemapItem", menuName = "Game Data/Barrel")]
public class TilemapItem : ScriptableObject
{
    public string fileType = ".jpg";
    public string fileName = "Barrel";

}