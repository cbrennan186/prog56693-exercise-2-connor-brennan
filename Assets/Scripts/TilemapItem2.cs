using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TilemapItem2", menuName = "Game Data/Steel Barrel")]
public class TilemapItem2 : TilemapItem
{
    public bool isHidden = true;
}
